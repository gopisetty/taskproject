package testcases;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import vendor.design.ProjectMethods;

public class LoginPage1 extends ProjectMethods {
	public LoginPage1() {
		PageFactory.initElements(driver,this);
	}

		@FindBy(how=How.ID,using="email") WebElement eleUsername;
		@FindBy(how=How.ID,using="password") WebElement elePassword;
		@FindBy(how=How.ID,using="buttonLogin")WebElement loginButton;
		public LoginPage1 enterUsername(String username)
		{
			eleUsername.sendKeys(username);
			return new LoginPage1();
		}
		public LoginPage1 enterPassword(String password) 
		{
		elePassword.sendKeys(password);
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
		return new LoginPage1();
		}
		
		public DashboardPage2 clickLogin()
		{
			loginButton.click();
            return new DashboardPage2();
		}
		
		
		
	
}

