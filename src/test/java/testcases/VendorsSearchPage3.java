package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import vendor.design.ProjectMethods;

public class VendorsSearchPage3 extends ProjectMethods {
	public VendorsSearchPage3()
	{
		PageFactory.initElements(driver, this);
	}
	
    @FindBy(how=How.ID,using="vendorTaxID") WebElement vendorId;
    @FindBy(how=How.XPATH,using="//button[@id='buttonSearch']") WebElement searchButton;
	public VendorsSearchPage3 SearchVendor(String Id)
	{
		vendorId.sendKeys(Id);
		driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
		return this;
	}
	public SearchResults4 clickSearch()
	{
		searchButton.click();
		return new SearchResults4();
	}

}
