package testcases;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import vendor.design.ProjectMethods;

public class SearchResults4 extends ProjectMethods{

	public SearchResults4()
	{
		PageFactory.initElements(driver, this);
	}
	
    
    //@FindBy(how=How.TAG_NAME,using="tr") WebElement rows;
    //@FindBy(how=How.TAG_NAME,using="td") WebElement columns;
   // List<WebElement> rows = table.findElements(By.tagName("tr"));
	List<WebElement> rows;
	public SearchResults4 SearchTable()
	{
		driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
		WebElement table = driver.findElement(By.className("table"));
		//driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
	    //WebElement firstrows = driver.findElement(By.id("buttonSearch"));
		List<WebElement> rows = table.findElements(By.tagName("tr"));

		System.out.println(rows.size());
		
		WebElement firstrows = rows.get(1);
		 List<WebElement> columns = firstrows.findElements(By.tagName("td"));
		 String vendorname = columns.get(0).getText();
		 driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
		 System.out.println(vendorname);		
		 return new SearchResults4();
	}
	
	
}
/*WebElement table= driver.findElementByClassName("table");


*/