package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import vendor.design.ProjectMethods;

public class DashboardPage2 extends ProjectMethods{
	public DashboardPage2() 
	{
	       PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using="//i[@class='fa fa-truck']") WebElement vendor;
	@FindBy(how=How.XPATH,using="//a[text()='Search for Vendor']") WebElement searchVendor;
	public VendorsSearchPage3 searchforVendor() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		Actions act = new Actions(driver);
		act.moveToElement(vendor).pause(3000).click(searchVendor).build().perform();
		return new VendorsSearchPage3();
	}
	
	
		
}
